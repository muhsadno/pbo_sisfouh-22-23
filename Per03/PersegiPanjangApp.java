class PersegiPanjangApp {
    
    public static void main(String[] args) {
        PersegiPanjang obj = new PersegiPanjang(4.0, 3.0);
        obj.cetakPersegiPanjang();

        System.out.println("\nPanggil constructor PersegiPanjang()");
        PersegiPanjang obj2 = new PersegiPanjang();
        obj2.cetakPersegiPanjang();
    }
}
