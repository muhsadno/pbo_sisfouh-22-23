import java.util.Scanner;

class PersegiPanjang {

    double panjang;
    double lebar;

    PersegiPanjang() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Input panjang = ");
        this.panjang = sc.nextDouble();
        System.out.print("Input lebar = ");
        this.lebar = sc.nextDouble();
        sc.close();
    }

    PersegiPanjang(double panjang, double lebar) {
        this.panjang = panjang;
        this.lebar = lebar;
    }

    void cetakPersegiPanjang(){
        System.out.println("Panjang = "+this.panjang);
        System.out.println("Lebar = "+this.lebar);
    }

}