package Per07;

public interface BangunDatar { //modifier interface hanya dapat public / default

    String jenis = "Bangun Datar";

    public void deskripsi();

    public double hitungLuas();

}
