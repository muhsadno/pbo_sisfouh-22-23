package Per07;

public class Budi implements Mahasiswa, Penduduk {

    String nama;
    String NIK;
    String JK;
    String NIM;
    public static void main(String[] args) {

        Budi budi = new Budi();
        budi.nama = "Budi Tampan";
        budi.NIK = "949494994";
        budi.JK = "Laki-Laki";
        budi.NIM = "H071231001";

        budi.deskripsiMhs();
        System.out.println("Jenis Kelamin = " + budi.JenisKelamin());
        budi.getNIM();

        budi.getNIK();
        Penduduk.cetakPekerjaan("Mahasiswa"); //static method dari interface
    }

    @Override
    public void getNIK() {
        System.out.println("NIK = " + NIK);
    }

    @Override
    public String JenisKelamin() {
        return JK;
    }

    @Override
    public void deskripsiMhs() {
        //System.out.println(nama +" adalah seorang mahasiswa " + Mahasiswa.super.getKampus());
        System.out.println(nama +" adalah seorang mahasiswa " + getKampus());
    }

    @Override
    public void getNIM() {
        System.out.println("NIM = " + NIM);        
    }

    @Override
    public String getKampus() {
        return "Universitas Hasanuddin Kampus Tamalanrea";
    }
    
}
