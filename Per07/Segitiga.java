package Per07;

public class Segitiga implements BangunDatar  {

    double alas, tinggi;

    public static void main(String[] args) {
        Segitiga sg = new Segitiga();
        sg.alas = 2.0;
        sg.tinggi = 4.0;
        sg.deskripsi();
        System.out.println("Luas = " + sg.hitungLuas());
        
        // atribut dari interface otomatis diubah oleh compiler menjadi public static final
        System.out.println("Jenis = " + BangunDatar.jenis);

    }
    
    //metode yang di-overide secara implisit diubah oleh compiler menjadi public abstract
    
    @Override
    public void deskripsi() {
        System.out.println("Ini bangun segitiga dengan alas "+ alas +" dan tinggi "+tinggi);
    }

    @Override
    public double hitungLuas() {
        return 0.5 * alas * tinggi;
    }

}
