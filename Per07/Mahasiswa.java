package Per07;

public interface Mahasiswa {

    void deskripsiMhs();

    void getNIM();

    default String getKampus() {
        return "Universitas Hasanuddin";
    }
    
}
