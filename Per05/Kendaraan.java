package Per05;

public class Kendaraan {

    private String namaKendaraan;

    Kendaraan() {
        System.out.println("Ini dari class Kendaraan");
    }
    
    public String getNamaKendaraan() {
        return namaKendaraan;
    }

    public void setNamaKendaraan(String namaKendaraan) {
        this.namaKendaraan = namaKendaraan;
    }

    protected void info() {
        System.out.println("Ini kendaraan");
    }
    
}
