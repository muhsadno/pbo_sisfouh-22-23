package Per05;

public class TestKendaraan {

    public static void main(String[] args) {
        Mobil mbl = new Mobil();
        mbl.info();
        mbl.cekInfo();
        Kendaraan kendaraan = new Kendaraan();
        kendaraan.info();

        System.out.println("");
        Kendaraan rubicon = new Mobil();
        rubicon.setNamaKendaraan("Rubicon");
        System.out.println("Jenis kendaraan = "+ rubicon.getNamaKendaraan());

        System.out.println("");
        Kendaraan CRV = new Mobil();
        CRV.setNamaKendaraan("Honda CRV");
        System.out.println("Jenis kendaraan = "+ CRV.getNamaKendaraan());

    }
    
}
