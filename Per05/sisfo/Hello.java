package Per05.sisfo;

public class Hello {

    private String kampus;

    public String getKampus() {
        return kampus;
    }

    public void setKampus(String kampus) {
        this.kampus = kampus;
    }

    protected void sayHi() {
        System.out.println("Hello "+ this.kampus);
    }

    protected void sayHi(String kampus) {
        System.out.println("Hello "+ kampus);
    }
    
}
