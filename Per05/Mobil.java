package Per05;

public class Mobil extends Kendaraan {

    Mobil() {
        System.out.println("Ini dari class Mobil");
    }

     @Override
     protected void info() {
         System.out.println("Ini kendaraan berjenis mobil");
     }

     public void cekInfo() {
        super.info();
     }

    void infoMobil() {
         System.out.println("Ini mobil");
    }
    
}