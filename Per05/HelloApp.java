package Per05;

import Per05.sisfo.Hello;

public class HelloApp extends Hello {

    public static void main(String[] args) {
        HelloApp obj = new HelloApp();
        obj.setKampus("Unhasku Bersatu Unhas Kuat");
        obj.sayHi();
        obj.sayHi("Unhas");
    }
    
}
