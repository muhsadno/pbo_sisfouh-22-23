package Per04;

public class Segitiga {

    private double alas;
    private double tinggi;
    private String jenis; //sama kaki, sama sisi, siku-siku
    
    public void setAlas(double alas) { 
        this.alas = alas;
    }  
    

    void setTinggi (double tinggi) {
        this.tinggi = tinggi;
    }

    void setJenis (String jenis) {
        this.jenis = jenis;
    }
    
    String getJenis() {
        return this.jenis;
    }

    Segitiga() {
        //empty constructor
    }

    Segitiga(double alas, double tinggi) {
        this.alas = alas;
        this.tinggi = tinggi;
    }

    void cetakLuas() {
        double luas = 0.5 * this.alas * this.tinggi;
        System.out.println("Luas = "+luas);
    }
    
}
