package Per06;

public class Kubus extends BangunRuang {

    double sisi;

    public static void main(String[] args) {
        Kubus kbs = new Kubus();
        kbs.sisi = 2.0;        
        System.out.println("Luas = "+ kbs.hitungLuas());
        System.out.println("Volume = "+ kbs.hitungVol());
    }

    @Override
    double hitungVol() {
        // TODO Auto-generated method stub
        return sisi*sisi*sisi;
    }

    @Override
    double hitungLuas() {
        // TODO Auto-generated method stub
       return 6*sisi*sisi;
    }
    
}
