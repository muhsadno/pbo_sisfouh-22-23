package Per06;

public abstract class Mobil {

    private String merkMobil;
    
    final public String getMerkMobil() {
        return merkMobil;
    }

    final public void setMerkMobil(String merkMobil) {
        this.merkMobil = merkMobil;
    }

    abstract void cetakWarna();
    //cetak warna mobil 

    abstract int tahunPembuatan(int tahun);
    //tahun pembuatan
    
}
