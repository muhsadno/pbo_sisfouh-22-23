package Per06;

public class Balok extends BangunRuang {

    double panjang, lebar, tinggi;
    public static void main(String[] args) {

        Balok balok = new Balok();
        balok.panjang = 3.0;
        balok.lebar = 2.0;
        balok.tinggi = 1.0;

        balok.cetakLuas();
        balok.cetakVol();
        
    }

    void cetakVol() {
        System.out.println("Volume = " + hitungVol());
    }

    void cetakLuas() {
        System.out.println("Volume = " + hitungVol());
    }

    @Override
    double hitungLuas() {
        // TODO Auto-generated method stub
        return 2*panjang*lebar + 2*panjang*tinggi + 2*tinggi*lebar;
    }
    @Override
    double hitungVol() {
        // TODO Auto-generated method stub
        return panjang * lebar * tinggi;
    }
    
}
