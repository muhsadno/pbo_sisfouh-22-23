package Per06;

public class Rubicon extends Mobil {

    String warna = "Hitam";
    public static void main(String[] args) {
        //Mobil kijang = new Mobil(); //error
        Rubicon rubicon = new Rubicon();
        rubicon.cetakWarna();
        int thnbuat = rubicon.tahunPembuatan(2020);
        System.out.println("Tahun pembuatan = " + thnbuat);

        rubicon.setMerkMobil("Rubicon Super");
        System.out.println("Merk = " + rubicon.getMerkMobil());
    }

    @Override
    void cetakWarna() {
        // TODO Auto-generated method stub
        System.out.println("Rubicon " + warna);
    }

    @Override
    int tahunPembuatan(int tahun) {
        return tahun;
    }

}
