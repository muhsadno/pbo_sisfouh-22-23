public class FlowControlIF {

    public static void main(String[] args) {
        // if
        int x = 7;
        if (x > 5) {
            System.out.println("Nilai Lebih dari 5");
        } else {
            System.out.println("Nilai Kurang dari 5");
        }

        char huruf = 'a';
        if ( x > 5 || huruf == 'a') {
            System.out.println("Huruf = a atau x > 5");
        } else if ( x > 5 && huruf == 'b' ) {
            System.out.println("Huruf b dan x > 5");
        } else {
            System.out.println("Tidak memenuhi kondisi");
        }

        // Seleksi kondisi menggunakan operator ternary
        String kampus = "Unhas";
        String cetak = (kampus == "Unhas") ? "Hello Unhas" : "bye";
        System.out.println(cetak);
        
    }
    
}
