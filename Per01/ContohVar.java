public class ContohVar {

    public static void main(String[] args) {
        /*
         * deklarasi variabel juga dapat menggunakan tipe 'var' jika
         * tipe datanya tidak ditentukan. Variabel yang dideklarasikan dengan 
         * keyword var harus langsung diberi nilai
         */
        var panjang = 1234;
        var kata = "Unhas";
        var lebarKunci = 12.4434;
        var hurufZ = 'Z';
        var status = true;                 
        
    }
    
}