public class ContohArgs {
    
    public static void main(String[] args) {
        String nama = args[0];
        String kelas = args[1];

        sayHi(nama,kelas);
        
    }

    static void sayHi(String nama, String kelas) {
        System.out.println("Hi "+nama+" dari kelas "+kelas);
    }
}
