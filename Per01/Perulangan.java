public class Perulangan {

    public static void main(String[] args) {
        
        //cetak bilangan 1,2,3, ... , n
        int n = 10;
        System.out.println("Cetak bilangan 1 2 3 ... "+ n);
        for (int i = 1; i <= n; i++) {
            System.out.print(i+" ");
        }
        System.out.println("\n");

        //cetak bilangan genap < m 
        int m = 20;
        System.out.println("Cetak bilangan genap < "+ m);
        for(int j=2; j < m; j=j+2) {
            System.out.print(j+" ");
        }
        System.out.println("\n");

        //cetak bilangan yang habis dibagi bilangan a dan b pada rentang n sampai 1
        int a = 3;
        int b = 5;
        n = 100;
        System.out.println("Cetak bilangan yang habis dibagi "+ a + " dan "+ b +" dari "+ n +" sampai 1");
        for (int i = n; i > 1; i--) {
            if (i % a == 0 && i % b == 0) {
                System.out.print(i+" ");
            }
        }

        //banyak bilangan asli kelipatan a dan b kurang dari atau sama dengan n
        int banyak = 0;
        int j = n;
        while (j > 1 && j <= n) {
            if (j % a == 0 && j % b == 0) {
                banyak++;
            }
            j--;
        }
        System.out.println("\nBanyaknya = "+ banyak);

    }
    
}
