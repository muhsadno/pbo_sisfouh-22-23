import java.util.Scanner;

public class latihan04 {
    public static void main (String[] args) {
        Scanner input = new Scanner(System.in);
        double r;  //r = jari-jari
        double luas, keliling;
        final double pi = 3.141592; //ini bernilai konstan
        //Input
        System.out.print("Input jari-jari = ");
        r = input.nextDouble();
        luas = pi*r*r;
        keliling = 2*pi*r;

        System.out.println("Luas = "+luas);
        System.out.println("Keliling = "+keliling);
        input.close();
        
    }
    
}
