package Per02;

public class Array {

    public static void main(String[] args) {

        // Array di Java seperti "List" di Python tetapi anggotanya harus bertipe sama
        // int[] myarray = {1, "kata", 100} //error karena tipe data anggotanya berbeda

        // Deklarasi Array satu dimensi
        String[] kampus = {"Unhas", "UNM", "UMI", "Unibos"};

        int[] bilprima = new int[5]; // array satu dimensi bertipe int dengan panjang 5
        bilprima[0] = 2;
        bilprima[1] = 3;
        bilprima[2] = 5;
        bilprima[3] = 7;
        bilprima[4] = 11;
        //bilprima[5] = 13; //error karena ukuran array hanya maksimal 5

        // iterasi elemen array
        System.out.println("Nama-nama kampus");
        for (int i = 0; i < 4; i++) { // batas i boleh menggunakan i < kampus.length
            System.out.println((i + 1) + ". " + kampus[i]);
        }

        // for (int i = 0; i < kampus.length; i++) { // batas i boleh menggunakan i <
        // kampus.length
        // System.out.println((i+1) +". "+kampus[i]);
        // }

        System.out.println("\n" + bilprima.length + " bilangan prima pertama");
        for (int x : bilprima) {
            System.out.print(x + " ");
        }

    }

}
