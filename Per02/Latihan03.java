package Per02;

// Satu file Java dapat memuat lebih dari satu class
// Jika modifier class adalah public maka nama file harus sama dengan public class-nya

class kelasA { //nama class bisa berbeda dengan nama file
    
    static void helloA() {
        System.out.println("Hello dari Kelas A");
    }
    
    public static void main(String[] args) {

        helloA();

        KelasB klsB = new KelasB();
        klsB.helloB();
    }
}


class KelasB {

    public static void main(String[] args) {
        System.out.println("Coba Kelas B");
    }

    void helloB() {
        System.out.println("Hello dari Kelas B");
    }

    static void sayHello(String namaKampus) {
        System.out.println("Hello "+namaKampus);
    }
}