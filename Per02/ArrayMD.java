package Per02;

public class ArrayMD {

    public static void main(String[] args) {

        String[][] kota = {
                { "Makassar", "Pare-Pare", "Kendari", "Palu" },
                { "Jakarta", "Bandung", "Surabaya" },
                { "Jayapura" }
        };

        int[][] matriks = new int[2][2]; // array 2 dimensi ukuran 2 x 2
        matriks[0][0] = 4;
        matriks[0][1] = 11;
        matriks[1][0] = 20;
        matriks[1][1] = 100;

        // int[][] matriks = { {4,11}, {20,100} };

        System.out.println("Nama-nama kota");
        for (String[] datakota : kota) {
            System.out.println("------");
            for (String namakota : datakota) {
                System.out.println(namakota);
            }
        }

        System.out.println("\nMatriks");
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                System.out.print(matriks[i][j]+" ");
            }
            System.out.println();
        }
    }
}