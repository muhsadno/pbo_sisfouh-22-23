package Per02;

public class Latihan01 {

    public static void main(String[] args) {

        Hello();
        HelloKampus("UMI");

        System.out.println();
        double bilrn = bilRandom();
        System.out.println("Bilangan random = "+ bilrn);
        System.out.println("Bilangan random (dibulatkan 2 angka)= "+ Math.round(bilrn*100.0) / 100.0);
    
        System.out.println("\nHitung Rataan");
        int[] data = { 1, 2, 3, 4, 5};
        System.out.println( rataanBilBulat(data) );

        System.out.println("Hitung rataan lagi");
        double rt = rataanBilB(1,2,3,4,5,6, 99,100);
        System.out.println(rt);
    }

    static void Hello() {
        System.out.println("Hello Unhas");
    }

    static void HelloKampus(String namaKampus) {
        System.out.println("Hello "+namaKampus);
    }

    static double bilRandom() {
        double br = Math.random();
        return br;
    }

    static double rataanBilBulat (int[] bulat) {
        int jumlah = 0;
        for (int x : bulat) {
            jumlah += x; // jumlah = jumlah + x
        }
        double rataan = jumlah / bulat.length;
        return rataan;
    }

    static double rataanBilB (int... bil) {
        double jumlah = 0;
        for (int x : bil) {
            jumlah += x;
            System.out.print(x+" ");
        }
        System.out.println();
        return jumlah / bil.length;
    }    
}
