package Per02;

import java.util.Scanner;

public class Rekursif {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Input bilangan bulat > 0 = ");
        int n = scan.nextInt();
        System.out.println("Jumlah 1 + 2 + ... + " + n + " = " + sum(n));
        scan.close();
    }

    public static int sum(int k) {
        if (k > 0) {
            return k + sum(k - 1);
        } else {
            return 0;
        }
    }

}
