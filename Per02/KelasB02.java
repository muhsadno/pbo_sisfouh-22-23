package Per02;

public class KelasB02 {
    
    // Kelas Kubus berisi atribut panjang sisi bertipe double
    // Buat metode menghitung Luas Permukaan, Volume

    public static void main(String[] args) {
        double sisi = 1.25;
        double luas = luasPermukaan(sisi);
        
        KelasB02 obj = new KelasB02();
        double vol = obj.volume(sisi);

        System.out.println("Luas permukaan = "+ luas);
        System.out.println("Volume = "+ vol);
    }

    static double luasPermukaan(double sisi) {
        return 6*sisi*sisi;
    }

    double volume(double sisi) {
        return sisi*sisi*sisi;
        //return Math.pow(sisi, 3);
    }

}
