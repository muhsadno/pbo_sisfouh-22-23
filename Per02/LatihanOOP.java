package Per02;

public class LatihanOOP {

    // Buat atribut untuk mahasiswa : namaLengkap, nim, ipk
    // Buat metode : sayHi(namaLengkap), cekIPK(ipk)
    // cekIPK > 3.00 -> output "Baik" else output "Kurang Baik"
    String namaLengkap;
    String nim;
    double ipk;

    void sayHi() {
        System.out.println("Hi " + this.namaLengkap);
    }

    void cekIPK() {
        if (this.ipk > 3) {
            System.out.println("IPK Baik");
        } else {
            System.out.println("IPK Kurang Baik");
        }
    }

}
