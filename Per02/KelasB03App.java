package Per02;

public class KelasB03App {

    public static void main(String[] args) {
        double panjang, lebar, tinggi;
        panjang = 3.25;
        lebar = 2.0;
        tinggi = 1.25;

        KelasB03 balok1 = new KelasB03();
        double luas = balok1.hitungLuas(panjang, lebar, tinggi);
        System.out.println("Luas = "+luas);

        KelasB03 balok2 = new KelasB03();
        balok2.panjang = 3.25;
        balok2.lebar = 1.25;
        balok2.tinggi = 1.0;
        double vol = balok2.hitungVol();
        System.out.println("Volume = "+vol);

        balok2.cetakBalok();
    }  
}
