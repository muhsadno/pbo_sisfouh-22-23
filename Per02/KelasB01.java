package Per02;

public class KelasB01 {

    public static void main(String[] args) {

        //Buat metode Hello yang punya parameter (nama, kampus) kemudian mencetak
        //Hello {nama}. {nama} berasal dari kampus {kampus}

        // Hello(Ani, Unhas) -> Hello Ani. Ani berasal dari kampus Unhas
        Hello("Budi", "Politeknik");
        
    }

    static void Hello (String nama, String kampus) {
        System.out.println("Hello "+ nama +". "+ nama +" berasal dari kampus"+ kampus );
    }
    
}
