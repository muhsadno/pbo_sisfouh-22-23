package Per02;

public class LatihanOOPApp {

    public static void main(String[] args) {
        LatihanOOP zakarie = new LatihanOOP();
        zakarie.namaLengkap = "Zakarie Jama Hasan";
        zakarie.nim = "H071221521";
        zakarie.ipk = 3.42;
        zakarie.sayHi();
        zakarie.cekIPK();

        LatihanOOP ani = new LatihanOOP();
        ani.namaLengkap = "Aniiiiii";
        ani.nim = "H071220001";
        ani.ipk = 2.71;
        ani.sayHi();
        ani.cekIPK();
    }
    
}
