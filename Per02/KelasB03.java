package Per02;

public class KelasB03 {

    //buat class yang berisi atribut panjang, lebar, tinggi balok
    //metodenya 
    // hitungLuas -> luas permukaan dari balok
    // hitungVol -> volume dari balok
    // cetakBalok -> print Balok mempunyai panjang = ?, lebar = ?, dan tinggi - ?
    
    double panjang, lebar, tinggi;

    double hitungLuas(double panjang, double lebar, double tinggi) {
        double luas;
        luas = 2*(panjang*lebar + panjang*tinggi + tinggi*lebar);
        return luas;
    }

    double hitungVol() {
        double volume;
        volume = this.panjang * this.lebar * this.tinggi;
        return volume;
    }

    void cetakBalok() {
        System.out.println("Balok mempunyai panjang = "+this.panjang);
        System.out.println("Balok mempunyai lebar = "+this.lebar);
        System.out.println("Balok mempunyai tinggi = "+this.tinggi);
    }
    
    
}
